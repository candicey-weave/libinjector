#include <stdio.h>
#include <stdlib.h>

#include "injector/injector.h"

int main(int argc, char *argv[]) {
    if (argc != 3) {
        fprintf(stderr, "Usage: %s <lib> <pid>\n", argv[0]);
        return 1;
    }

    char *lib = argv[1];
    pid_t pid = atoi(argv[2]);

    printf("Injecting %s into pid %d\n", lib, pid);

    injector_t *injector;

    injector_attach(&injector, pid);

    if (injector_inject(injector, lib, NULL) != 0) {
        const char *injectorError = injector_error();
        fprintf(stderr, "ERROR: %s\n", injectorError);
    } else {
        printf("Successfully injected library\n");
    }

    injector_detach(injector);

    return 0;
}
